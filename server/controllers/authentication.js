'use strict';

const passport = require('passport');

exports.logout = function(req, res) {
    if(req.isAuthenticated()){
        req.logout();
        res.json({message: 'log out'});
    } else {
        res.status(409).json({message: 'already logged out'});
    }


};
exports.isAuthenticated = function(req, res, next) {
    if (!req.isAuthenticated()) {
        res.status(403).json({message: 'not authorized'});
    }
    else {
        next();
    }
};
exports.facebookAuth = function(req,res,next) {
    passport.authenticate('facebook')(req, res, next);
};
exports.facebookAuthCallback = function(req,res,next){
    console.log('authentication via google done');
    passport.authenticate( 'facebook', {
        successRedirect : '/',
        failureRedirect : '/home'
    })(req, res, next);
};



