'use strict';

const cloudinary = require('cloudinary');

const geolib = require('geolib');

const moment = require('moment');

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Relation = mongoose.model('Relation');

exports.afterLogin = function (req, res) {
    let userData = req.body;
    User.findOne({facebookId: req.body.id},function (err,data) {
        if(err) throw err;
        if(data == null){
            let newUser = new User();
            newUser.picture = userData.picture;
            newUser.locale = userData.locale;
            newUser.first_name = userData.first_name;
            newUser.last_name = userData.last_name;
            newUser.email = userData.email;
            newUser.facebookId = userData.id;
            newUser.gender = userData.gender;
            newUser.cover = userData.cover;
            newUser.name = userData.name;
            newUser.age_range = userData.age_range;
            newUser.lat = userData.lat;
            newUser.long = userData.long;
            // line
            newUser.link = userData.link;
            newUser.user_birthday = moment(userData.user_birthday, "MM/DD/YYYY").toDate().getTime();
            newUser.user_education_history = userData.user_education_history;
            newUser.user_work_history = userData.user_work_history;
            newUser.save(function (err) {
                if(err) throw err;
                console.log('saved user');
                res.json({message: 'saved user'})
            });
        }else{
            data.picture = userData.picture ? userData.picture : data.picture ;
            data.locale = userData.locale ? userData.locale : data.locale ;
            data.first_name = userData.first_name ? userData.first_name : data.first_name;
            data.last_name = userData.last_name ? userData.last_name : data.last_name;
            data.gender = userData.gender ? userData.gender : data.gender;
            data.cover = userData.cover ? userData.cover : data.cover;
            data.name = userData.name ? userData.name : data.name;
            data.age_range = userData.age_range ? userData.age_range : data.age_range;
            data.lat = userData.lat ? userData.lat : data.long;
            data.long = userData.long ? userData.lat : data.long;
            data.link = userData.link ? userData.link : data.link;
            data.user_birthday = userData.user_birthday ? moment(userData.user_birthday, "MM/DD/YYYY").toDate().getTime() : data.user_birthday;
            data.user_education_history = userData.user_education_history ? userData.user_education_history : data.user_education_history;
            data.user_work_history = userData.user_work_history ? userData.user_work_history : data.user_work_history;
            data.save(function (err) {
                if(err) throw err;
                console.log('saved user');
                res.json({message: 'saved user'})
            });
        }
    });
};

exports.editUser = function (req, res) {
    let query = {facebookId: req.body.id};
    let userData = req.body;
    if(req.file){
        cloudinary.uploader.upload(req.file.path, function(result) {
            User.findOne(query,function (err,data) {
                if (err) throw err;
                data.picture = result.url;
                data.locale = userData.locale ? userData.locale : data.locale;
                data.first_name = userData.first_name ? userData.first_name : data.first_name;
                data.last_name = userData.last_name ? userData.last_name : data.last_name;
                data.gender = userData.gender ? userData.gender : data.gender;
                data.cover = userData.cover ? userData.cover : data.cover;
                data.name = userData.name ? userData.name : data.name;
                data.age_range = userData.age_range ? userData.age_range : data.age_range;
                data.link = userData.link ? userData.link : data.link;
                data.lat = userData.lat ? userData.lat : data.lat;
                data.long = userData.long ? userData.long : data.long;
                data.user_birthday = userData.user_birthday ? userData.user_birthday : data.user_birthday;
                data.user_education_history = userData.user_education_history ?  userData.user_education_history : data.user_education_history;
                data.user_work_history = userData.user_work_history ? userData.user_work_history : data.user_work_history;
                data.save(function (err, user) {
                    if(err) throw err;
                    console.log('User updated with image');
                    res.json({success: 'updated user'});
                });
            });
        });
    } else {
        User.findOne(query,function (err,data) {
            if (err) throw err;
            data.picture = result.url;
            data.locale = userData.locale ? userData.locale : data.locale;
            data.first_name = userData.first_name ? userData.first_name : data.first_name;
            data.last_name = userData.last_name ? userData.last_name : data.last_name;
            data.gender = userData.gender ? userData.gender : data.gender;
            data.cover = userData.cover ? userData.cover : data.cover;
            data.name = userData.name ? userData.name : data.name;
            data.age_range = userData.age_range ? userData.age_range : data.age_range;
            data.link = userData.link ? userData.link : data.link;
            data.lat = userData.lat ? userData.lat : data.lat;
            data.long = userData.long ? userData.long : data.long;
            data.user_birthday = userData.user_birthday ? userData.user_birthday : data.user_birthday;
            data.user_education_history = userData.user_education_history ?  userData.user_education_history : data.user_education_history;
            data.user_work_history = userData.user_work_history ? userData.user_work_history : data.user_work_history;
            data.save(function (err, user) {
                if(err) throw err;
                console.log('User updated');
                res.json({success: 'updated user'});
            });
        });
    }
};
exports.changePhoto = function (req, res) {
    let query = {facebookId: req.body.id};
    if(req.file){
        cloudinary.uploader.upload(req.file.path, function(result) {
            User.findOne(query,function (err,data) {
                if (err) throw err;
                data.image = result.url;
                console.log('Updated image');
                res.json({success: 'Updated image'});
            });
        });
    } else {
        res.status(400).json({error: 'No image attached'});
    }
};

exports.getUserInfo = function (req, res) {
  let userId = req.params.id;
  User.findOne({facebookId: userId},function (err, user) {
      if(err) throw err;
      if(user == null){
          res.status(404).json({error: 'Not found'});
      }else{
          res.json({
              email: user.email,
              image: user.image,
              updated: user.updated,
              created: user.created,
              locale: user.locale,
              first_name: user.first_name,
              last_name: user.last_name,
              timezone: user.timezone,
              lat: user.lat,
              long: user.long
          });
      }
  });
};
exports.review = function (req, res) {
  let who = req.body.user;
  let whom = req.body.target;
  let result = req.body.result;
  Relation.findOne({who ,whom},function (err, relation) {
      if(err) throw err;
      if(relation == null){
          let rel = new Relation();
          rel.who = who;
          rel.whom = whom;
          rel.result = result;
          rel.save(function (err) {
              if(err) throw err;
              res.json({message: 'successfully saved'});
          });
      }else{
          relation.result = result;
          relation.save(function (err) {
              if(err) throw err;
              res.json({message: 'successfully saved'});
          });
      }
  });
};
exports.userDistance = function (req, res) {
    let userid  = req.body.userid;
    let target  = req.body.target;
    User.findOne({facebookId: [userid,target]},function (err, users) {
        if(err) throw err;
        if(users[0]){
            if(users[1]){
                res.json(getDistance(users[0],users[1]));
            }else{
                res.status(400).json({error: 'User with ' + target + ' not found'});
            }
        }else{
            res.status(400).json({error: 'User with ' + userid + ' not found'});
        }
    });
};

let getDistance = function (user1,user2) {
    return {
        distance: geolib.getDistance(
            {latitude: user1.lat, longitude: user1.long},
            {latitude: user2.lat, longitude: user2.long}
        )
    }
};

exports.relatedUsers = function (req, res) {
    // vzdálenost
    const YEAR = 60 * 60 * 24 * 365 * 1000;
    const NOW = new Date().getTime();
    let lookingForDistance = false;
    let query = {};
    let distance = '';
    let userlat = '';
    let userlong = '';
    if(req.body.distance){
        lookingForDistance = true;
        distance = req.body.distance;
        userlat = req.body.lat;
        userlong = req.body.long;
    }
    if(req.body.gender){
        query.gender = req.body.gender;
    }
    if(req.body.age){
        let ageRateTop = NOW - (req.body.ageTop * YEAR);
        let ageRateDown = NOW - (req.body.ageDown * YEAR);
        console.log('looking by query',query);
        User
            .find(query)
            .where('user_birthday').gt(ageRateTop).lt(ageRateDown)
            .exec(function (err,data){
                if(err) throw err;
                console.log('done - related',data.length);
                if(lookingForDistance){
                    res.json(finalDecide(data,distance,userlat,userlong));
                }else{
                    res.json({data: data});
                }
            });
    }else{
        console.log('looking by query',query);
        User
            .find(query)
            .exec(function (err,data){
                if(err) throw err;
                console.log('done - related',data.length);
                if(lookingForDistance){
                    res.json(finalDecide(data,distance,userlat,userlong));
                }else{
                    res.json({data: data});
                }
            });
    }

};

let finalDecide = function (data, distance, userlat,userlong) {
    let result = [];
    data.forEach(function (user, index, array) {
        if(user.lat && user.long){
            let computedDistance = getDistance({lat: userlat, long: userlong},user);
            console.log('compar',{lat: userlat, long: userlong},user,computedDistance,distance);
            if(computedDistance.distance < distance){
                let copyUser = Object.assign({}, user.toObject());
                copyUser.distance = computedDistance.distance;
                result.push(copyUser);
            }
        }
    });
    return result;
};