module.exports = function(app) {
    const mongoose = require('mongoose');
    const Message = mongoose.model('Message');
    let http = require('http').Server(app);
    let io = require('socket.io')(http);
    io.on('connection', function(socket){
        console.log('user connected');
        socket.on('disconnect', function(who){
            console.log('user disconnected',who);
        });

        socket.on('NEW MESSAGE', function (data) {
            // we tell the client to execute 'new message'
            console.log('username',socket.id);
            let newMessage = new Message();
            newMessage.usernameFrom = data.usernameFrom;
            newMessage.usernameTo = data.usernameTo;
            newMessage.userIdFrom = data.userIdFrom;
            newMessage.userIdTo = data.userIdTo;
            newMessage.message = data.message;
            newMessage.save(function (err) {
                if(err) throw err;
                socket.emit('MESSAGE', {
                    usernameTo: data.usernameTo,
                    usernameFrom: data.usernameFrom,
                    userIdTo: data.userIdTo,
                    userIdFrom: data.userIdFrom,
                    message: data.message
                });
            });
        });
    });

    // start the server
    http.listen(app.get('port'), function() {
        console.log('Express server listening on port', app.get('port'));
    });

    module.exports.IO = io;
};
