const mongoose = require('mongoose');

module.exports.init = function () {
    let userSchema = new mongoose.Schema({
        email:  { type: String, unique: true },
        facebookId: { type: String, unique: true },
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
        gender: String,
        locale: String,
        first_name: String,
        last_name: String,
        picture: String,
        lat: Number,
        long: Number,
        cover: String,
        name: String,
        age_range: String,
        link: String,
        user_birthday: Number,
        user_education_history: String,
        user_work_history: String,
    });
    userSchema.pre('save', function(next) {
        this.updated = Date.now();
        next();
    });

    let User = mongoose.model('User', userSchema);
};

// get data after login
// edit data
// heroku
