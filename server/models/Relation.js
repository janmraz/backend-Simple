const mongoose = require('mongoose');

module.exports.init = function () {
    let relationSchema = new mongoose.Schema({
        who: String,
        whom: String,
        result: String,
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now },
    });
    relationSchema.pre('save', function(next) {
        this.updated = Date.now();
        next();
    });

    let Relation = mongoose.model('Relation', relationSchema);
};
