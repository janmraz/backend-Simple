const mongoose = require('mongoose');

module.exports.init = function () {
    let messageSchema = new mongoose.Schema({
        usernameTo: String,
        usernameFrom: String,
        userIdTo: String,
        userIdFrom: String,
        message: String
    });

    let Message = mongoose.model('Message', messageSchema);
};
