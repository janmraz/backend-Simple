'use strict';

const cookieParser = require('cookie-parser');

const cors = require('cors');

module.exports = function (app) {

    // use cookierParser
    app.use(cookieParser());

    // services
    const passport = require('passport');

    // session false as we are not using cookies, using tokens
    const requireAuth = passport.authenticate('jwt', { session: false });

    // allow requests from cross origin
    app.use(cors());

    // ---------- API -------------
    // app.get('/',function (req, res) {
    //     res.sendFile('/Users/janmraz/Desktop/backend/server/routes/index.html');
    // });
    // USER ROUTES
    const userRoutes = require('./user_routes');
    app.use('/api', userRoutes);

    // PROTECTED ROUTES
    // protected route
    app.route('/protected')
        .get(requireAuth, function(req, res) {
            res.send({ message: 'Authenticated' });
        });
};
