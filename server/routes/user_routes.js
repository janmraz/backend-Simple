const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const multer  = require('multer');
let upload = multer({ dest: './public/img/avatars/',
    inMemory: true });

// controllers
const Authentication = require('../controllers/authentication');
const UserController = require('../controllers/userController');

// ROUTES -----------------------------------------------------


let auth = Authentication.isAuthenticated;

// router.get('/',function(req, res) {
//     res.sendFile('./index.html');
// });

// router.get('/facebook/auth',Authentication.facebookAuth);
// router.get('/facebook/auth/callback',Authentication.facebookAuthCallback);
// router.get('/logout',Authentication.logout);


// AFTER LOGIN
router.post('/login',UserController.afterLogin);

// EDIT USER
router.post('/user',upload.single('image'), jsonParser,UserController.editUser);

// USER INFO
router.get('/user/:id', UserController.getUserInfo);

// USER CHANGE PHOTO
router.post('/user/photo',UserController.changePhoto);

// RELATE
router.post('/review', UserController.review);

// DISTANCE
router.post('/distance', UserController.userDistance);

// RELATED USERS
router.post('/relate', UserController.relatedUsers);


module.exports = router;
