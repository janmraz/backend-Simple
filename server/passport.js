const passport = require('passport');
const User = require('mongoose').model('User');
const FacebookStrategy = require('passport-facebook').Strategy;
let config = require('../config');
let callbackUrl;

module.exports = function(env) {
    callbackUrl = config[env].callbackURL;
    passport.use(new FacebookStrategy({
            clientID        : config.clientID,
            clientSecret    : config.clientSecret,
            callbackURL     : callbackUrl,
            profileFields: ['id','cover','picture', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified'],
        },
        function(accessToken, refreshToken, profile, done) {
            // User.findOne won't fire until we have all our data back from Facebook
            // try to find the user based on their google id
            console.log('looking for data',accessToken,profile);
            User.findOne({ facebookId: profile.id }, function(err, user) {
                if (err) return done(err);
                if (user) {
                    console.log('log in');
                    // if a user is found, log them in
                    return done(null, user);
                } else {
                    console.log('creating user',profile);
                    let newUser = new User();
                    // set all of the relevant information
                    newUser.email =    '';
                    newUser.name =  profile.first_name + ' ' + profile.last_name;
                    newUser.facebookId = profile.id;
                    newUser.gender = profile.gender;
                    newUser.locale = profile.locale;
                    newUser.first_name = profile.first_name;
                    newUser.last_name = profile.last_name;
                    newUser.timezone = profile.timezone;
                    // save the user
                    newUser.save(function(err) {
                        if (err) throw err;
                        console.log('user saved');
                        return done(null, newUser);
                    });
                }
            });
    }));
    passport.serializeUser(function(user, done) {
        if (user) {
            return done(null, user._id);
        }
    });

    passport.deserializeUser(function(id, done) {
        User.findOne({_id: id}).exec(function(err, user) {
            if (err) {
                console.log('Error loading user: ' + err);
                return;
            }

            if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    });
};
